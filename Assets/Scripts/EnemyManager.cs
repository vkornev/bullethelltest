﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Enemy
{
    public GameObject gameObject;
    public Collider collider;
    public NavMeshAgent navMeshAgent;
    public Renderer renderer;
    public float life;
    public float currentLife;
    public Scrollbar uiLifeBar;

    public Enemy(GameObject enemyObj, float life)
    {
        gameObject = enemyObj;
        renderer = enemyObj.GetComponent<Renderer>();
        collider = enemyObj.GetComponent<Collider>();
        this.life = life;
        currentLife = life;
        navMeshAgent = enemyObj.GetComponent<NavMeshAgent>();
        uiLifeBar = enemyObj.GetComponentInChildren<Scrollbar>();
    }

    public void Spawn(Vector3 position, Quaternion rotation)
    {
        gameObject.SetActive(true);
        renderer.material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        gameObject.transform.position = position;
        gameObject.transform.rotation = rotation;
        navMeshAgent.Warp(position);
    }

    public void Damage(float damage, Action<Enemy> afterDamageCallback)
    {
        currentLife -= damage;
        uiLifeBar.size = currentLife / life;
        afterDamageCallback?.Invoke(this);
    }

    public void Suicide(Action<Enemy> afterDamageCallback)
    {
        Damage(life, afterDamageCallback);
    }

    public void Reset()
    {
        currentLife = life;
        uiLifeBar.size = 1f;
        gameObject.SetActive(false);
    }
}

public class EnemyManager : MonoBehaviour
{
    public int ActiveEnemiesCount => activeEnemies.Count;

    public event Action OnEnemyKilled;

    private static float DefaultLife = 100f;

    [SerializeField] private GameController gameCtrl = null;
    [SerializeField] private Transform target = null;
    [SerializeField] private GameObject enemyPrefab = null;

    private Queue<Enemy> enemyPool = new Queue<Enemy>();
    private Dictionary<Collider, Enemy> activeEnemies = new Dictionary<Collider, Enemy>();

    public void Hit(Collider collider, float damage)
    {
        if (!activeEnemies.ContainsKey(collider))
            return;

        activeEnemies[collider].Damage(damage, AfterDamage);
    }

    public void SpawnEnemy(Vector3 position, Quaternion rotation)
    {
        Enemy enemy;
        if (enemyPool.Count > 0)
            enemy = enemyPool.Dequeue();
        else
            enemy = AddEnemy(position, rotation);

        enemy.Spawn(position, rotation);
        activeEnemies.Add(enemy.collider, enemy);
    }

    public void Collided(Collider collider)
    {
        if (!activeEnemies.ContainsKey(collider))
            return;

        activeEnemies[collider].Suicide(AfterSuicide);
    }

    public void StopAllAgents()
    {
        foreach (Collider key in activeEnemies.Keys)
            if (activeEnemies[key].gameObject.activeSelf)
                activeEnemies[key].navMeshAgent.isStopped = true;
    }

    private void Update()
    {
        if (!target) return;

        foreach (Collider key in activeEnemies.Keys)
            if (activeEnemies[key].gameObject.activeSelf)
                activeEnemies[key].navMeshAgent.SetDestination(target.position);
    }

    private Enemy AddEnemy(Vector3 position, Quaternion rotation)
    {
        GameObject enemyObj = Instantiate(enemyPrefab, position, rotation);
        Enemy enemy = new Enemy(enemyObj, DefaultLife);

        return enemy;
    }

    private void AfterDamage(Enemy enemy)
    {
        if (enemy.currentLife <= 0f)
        {
            gameCtrl.AddScore();
            activeEnemies.Remove(enemy.collider);
            enemy.Reset();
            enemyPool.Enqueue(enemy);

            OnEnemyKilled?.Invoke();
        }
    }

    private void AfterSuicide(Enemy enemy)
    {
        gameCtrl.AddScore();
        activeEnemies.Remove(enemy.collider);
        enemy.Reset();
        enemyPool.Enqueue(enemy);
    }
}
