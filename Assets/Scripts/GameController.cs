﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public int Score
    {
        get { return score; }
    }

    [SerializeField] private Collider[] spawnAreas;
    [SerializeField] private float spawnDelay;
    [SerializeField] private EnemyManager enemyManager = null;

    public float progressiveDelay;
    private float spawnTimer;
    private int score = 0;

    public void ProjectileHit(Collider collider, float damage)
    {
        enemyManager.Hit(collider, damage);
    }

    public void AddScore()
    {
        score++;
    }

    public void NotifyDeath()
    {
        enemyManager.StopAllAgents();

        SceneManager.LoadScene(0);
    }

    private void Start()
    {
        progressiveDelay = spawnDelay;
    }

    private void OnEnable()
    {
        InputManager.OnEscape += Application.Quit;
    }

    private void OnDisable()
    {
        InputManager.OnEscape -= Application.Quit;
    }

    private void Update()
    {
        if (spawnTimer <= 0f)
            SpawnEnemy();

        spawnTimer -= Time.deltaTime;
    }

    private void SpawnEnemy()
    {
        spawnTimer = progressiveDelay;

        progressiveDelay *= 0.95f;

        if (enemyManager.ActiveEnemiesCount >= 20)
            return;

        int spawnAreaId = Random.Range(0, spawnAreas.Length);

        float x = Random.Range(spawnAreas[spawnAreaId].bounds.center.x - spawnAreas[spawnAreaId].bounds.extents.x,
            spawnAreas[spawnAreaId].bounds.center.x + spawnAreas[spawnAreaId].bounds.extents.x);
        float y = Random.Range(spawnAreas[spawnAreaId].bounds.center.y - spawnAreas[spawnAreaId].bounds.extents.y,
            spawnAreas[spawnAreaId].bounds.center.y + spawnAreas[spawnAreaId].bounds.extents.y);
        float z = Random.Range(spawnAreas[spawnAreaId].bounds.center.z - spawnAreas[spawnAreaId].bounds.extents.z,
            spawnAreas[spawnAreaId].bounds.center.z + spawnAreas[spawnAreaId].bounds.extents.z);

        enemyManager.SpawnEnemy(new Vector3(x, y, z), spawnAreas[spawnAreaId].transform.rotation);
    }
}
