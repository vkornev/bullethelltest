﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform lookAtObj = null;
    [SerializeField] private Vector3 shift;

    void Update()
    {
        if (!lookAtObj) return;

        Quaternion newRot = Quaternion.LookRotation((lookAtObj.position - transform.position).normalized);
        Vector3 newPos = lookAtObj.position - lookAtObj.forward * shift.z + lookAtObj.up * shift.y + lookAtObj.right * shift.x;

        transform.position = Vector3.Lerp(transform.position, newPos, 10f * Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation, newRot, 10f * Time.deltaTime);
    }
}
