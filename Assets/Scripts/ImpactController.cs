﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class ImpactController : MonoBehaviour
{
    public float CurrentLife => currentLife;
    public float Life => life;

    [SerializeField] private float life = 0f;
    [SerializeField] private int score = 0;
    [SerializeField] private EnemyManager enemyManager = null;
    [SerializeField] private GameObject destructionEffect = null;
    [SerializeField] private GameController gameController = null;

    private float currentLife;
    private float collisionDamage;
    private float lifeRestoreAmount;

    private void Start()
    {
        currentLife = life;
        collisionDamage = life * .1f;
        lifeRestoreAmount = life * .05f;
    }

    private void OnEnable()
    {
        enemyManager.OnEnemyKilled += RestoreLife;
    }

    private void OnDisable()
    {
        enemyManager.OnEnemyKilled -= RestoreLife;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!LayerMask.LayerToName(other.gameObject.layer).Equals("Enemy"))
            return;

        enemyManager.Collided(other);

        currentLife -= collisionDamage;

        if (currentLife <= 0f)
            Death();
    }

    private void RestoreLife()
    {
        if (Random.Range(0f, 1f) < .1f)
            currentLife += lifeRestoreAmount;

        currentLife = Mathf.Clamp(currentLife, 0f, life);
    }

    private void Death()
    {
        currentLife = 0f;
        Collider mainCollider = GetComponent<Collider>();
        if (mainCollider)
            mainCollider.enabled = false;

        gameController.NotifyDeath();

        GameObject effect = Instantiate(destructionEffect, transform.position, transform.rotation);
        Destroy(effect, 5f);

        gameObject.SetActive(false);
    }
}
