﻿using UnityEngine;
using UnityEngine.UI;

public class UIPlayer : MonoBehaviour
{
    [SerializeField] private Scrollbar healthBar;
    [SerializeField] private Text scoreValueText;
    [SerializeField] private ImpactController playerIC;

    [SerializeField] private GameController gameController = null;

    void Update()
    {
        if (playerIC)
        {
            healthBar.size = playerIC.CurrentLife / playerIC.Life;
        }

        scoreValueText.text = gameController.Score.ToString();
    }
}
