﻿using System;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static event Action<float, float> OnMove;
    public static event Action<bool> OnFiring;
    public static event Action<Vector3> OnPointerChange;
    public static event Action OnEscape;

    private void Update()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");
        OnMove?.Invoke(vertical, horizontal);

        if (Input.GetButtonDown("Fire1"))
            OnFiring?.Invoke(true);
        if (Input.GetButtonUp("Fire1"))
            OnFiring?.Invoke(false);

        if (Input.GetButtonDown("Cancel"))
            OnEscape?.Invoke();

        OnPointerChange?.Invoke(Input.mousePosition);
    }
}
