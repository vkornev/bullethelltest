﻿using UnityEngine;

public class MoveController : MonoBehaviour
{
    [SerializeField] private CharacterController charCtrl = null;
    [SerializeField] private float maxSpeed = 0f;
    [SerializeField] private float turnSpeed = 0f;

    private void Start()
    {
        charCtrl.detectCollisions = true;
    }

    private void OnEnable()
    {
        InputManager.OnMove += MoveChange;
    }

    private void OnDisable()
    {
        InputManager.OnMove -= MoveChange;
    }

    private void MoveChange(float vertical, float horizontal)
    {
        charCtrl.SimpleMove(new Vector3(horizontal, 0f, vertical) * maxSpeed);
    }
}