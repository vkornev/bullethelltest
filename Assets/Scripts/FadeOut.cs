﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : MonoBehaviour
{
	private Renderer myRenderer;
	private Rigidbody myRigidbody;
	private bool isDone = true;

	private void OnEnable()
	{
		myRenderer = GetComponent<Renderer>();
		myRigidbody = GetComponent<Rigidbody>();
		isDone = true;
	}

	private void Update()
	{
		if (isDone) return;

		Color c = myRenderer.material.GetColor("_Color");
		c.a -= Time.deltaTime;
		myRenderer.material.SetColor("_Color", c);

		if (c.a <= 0f)
		{
			isDone = true;
			Destroy(gameObject);
		}
	}

	private void FixedUpdate()
	{
		if (myRigidbody.velocity == Vector3.zero)
			isDone = false;
	}
}
