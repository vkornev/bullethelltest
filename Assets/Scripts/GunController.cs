﻿using System.Collections.Generic;
using UnityEngine;

public class Projectile
{
    public GameObject gameObject;
    public Transform transform;
    public Rigidbody rigidbody;
    public float speed;
    public float damage;
    public Vector3 savedPosition;

    public void Spawn(Vector3 position, Quaternion rotation)
    {
        rigidbody.velocity = Vector3.zero;
        transform.position = position;
        savedPosition = position;
        transform.rotation = rotation;
        gameObject.SetActive(true);
        rigidbody.AddForce(transform.forward * speed);
    }

    public void Despawn()
    {
        gameObject.SetActive(false);
        rigidbody.velocity = Vector3.zero;
    }
}

public class GunController : MonoBehaviour
{
    [SerializeField] private GameController gameCtrl = null;
    [SerializeField] private Transform turret = null;
    [SerializeField] private GameObject projectilePrefab = null;
    [SerializeField] private Transform spawnLocation = null;
    [SerializeField] private LayerMask affectedLayers;
    [SerializeField] private float fireDelay = 0f;
    [SerializeField] private float damage = 0f;
    [SerializeField] private float projectileSpeed = 0f;

    private Queue<Projectile> projectilesPool = new Queue<Projectile>();
    private List<Projectile> activeProjectiles = new List<Projectile>();
    private float timer = 0f;
    private bool isFiring = false;
    private Camera myCamera;

    private void Start()
    {
        myCamera = Camera.main;
    }

    private void OnEnable()
    {
        InputManager.OnFiring += SetFiring;
        InputManager.OnPointerChange += AdjustTurret;
    }

    private void OnDisable()
    {
        InputManager.OnFiring -= SetFiring;
        InputManager.OnPointerChange -= AdjustTurret;
    }

    private void Update()
    {
        timer -= Time.deltaTime;

        if (isFiring && timer <= 0f)
        {
            SpawnProjectile(spawnLocation.position, spawnLocation.rotation);
            timer = fireDelay;
        }
    }

    private void FixedUpdate()
    {
        Projectile[] projectiles = activeProjectiles.ToArray();
        foreach (Projectile projectile in projectiles)
        {
            RaycastHit hit;

            Debug.DrawLine(projectile.savedPosition, projectile.transform.position, Color.red, 5f);

            if (Physics.Linecast(projectile.savedPosition, projectile.transform.position, out hit, affectedLayers))
            {
                gameCtrl.ProjectileHit(hit.collider, projectile.damage);

                projectile.Despawn();
                activeProjectiles.Remove(projectile);
                projectilesPool.Enqueue(projectile);
            }

            projectile.savedPosition = projectile.transform.position;
        }
    }

    private void SetFiring(bool value)
    {
        isFiring = value;
    }

    private void AdjustTurret(Vector3 mousePos)
    {
        Ray ray = myCamera.ScreenPointToRay(mousePos);
        if (Physics.Raycast(ray, out RaycastHit hit, 1000))
        {
            Vector3 worldPos = hit.point;
            worldPos.y = turret.position.y;
            turret.LookAt(worldPos, Vector3.up);
        }
    }

    private void SpawnProjectile(Vector3 position, Quaternion rotation)
    {
        Projectile projectile;
        if (projectilesPool.Count > 0)
            projectile = projectilesPool.Dequeue();
        else
            projectile = AddProjectile(position, rotation);

        projectile.Spawn(position, rotation);
        activeProjectiles.Add(projectile);
    }

    private Projectile AddProjectile(Vector3 position, Quaternion rotation)
    {
        GameObject bulletObj = Instantiate(projectilePrefab, spawnLocation.position, spawnLocation.rotation);
        Projectile projectile = new Projectile()
        {
            gameObject = bulletObj,
            transform = bulletObj.transform,
            rigidbody = bulletObj.GetComponent<Rigidbody>(),
            damage = damage,
            speed = projectileSpeed
        };

        return projectile;
    }
}
